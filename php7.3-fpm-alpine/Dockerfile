FROM php:7.3-fpm-alpine

RUN apk update \
    && apk add --no-cache --update libmemcached-libs zlib libzip libpng \
    && apk add --no-cache --update --virtual .phpize-deps $PHPIZE_DEPS openssl-dev g++ make zlib-dev libzip-dev libpng-dev \
    && apk add --no-cache --update --virtual .memcached-deps zlib-dev libmemcached-dev cyrus-sasl-dev \
    && pecl install igbinary \
    && ( \
        pecl install --nobuild memcached && \
        cd "$(pecl config-get temp_dir)/memcached" && \
        phpize && \
        ./configure --enable-memcached-igbinary && \
        make -j$(nproc) && \
        make install && \
        cd /tmp/ \
    ) \
    && pecl install redis-4.3.0 \
    && pecl install xdebug-2.7.0 \
    && docker-php-ext-install pdo_mysql zip gd \
    && docker-php-ext-enable redis xdebug pdo_mysql igbinary memcached \
    && curl --silent --show-error https://getcomposer.org/installer | php -- --install-dir=/usr/bin --filename=composer \
    && apk del --purge autoconf .memcached-deps .phpize-deps

USER www-data
